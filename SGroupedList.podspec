Pod::Spec.new do |spec|
  spec.name          = "SGroupedList"
  spec.version       = "2.3.2"
  spec.summary       = "Framework to handle UITableView in a easy way"
  spec.description   = "Framework to handle UITableView in a easy way"
  spec.homepage      = "https://gitlab.com/DavidGiordana/sgroupedlist-ios"
  spec.license       = { :type => 'MIT' }
  spec.author        = { "David" => "davidgiordana00@gmail.com" }
  spec.platform      = :ios, "11.0"
  spec.swift_version = '5.0'
  spec.source        = { :git => "https://gitlab.com/DavidGiordana/sgroupedlist-ios.git", :tag => "1.0.0" }
  spec.source_files  = "SGroupedList/SGL/**/*"
  spec.framework     = "UIKit"
  spec.requires_arc  = true  
end
