import UIKit

open class SGLCell: UITableViewCell, SGLThemeSettable {

    // MARK: - Data
    
    class var cellID: String {
        return "SGLCell"
    }
    
    public private(set) var cellData: SGLCellData!
    
    open var theme: SGLTheme!
    
    public private(set) weak var model: SGLModel!
    
    // MARK: - Interface
    
    open func setData(cellData: SGLCellData, model: SGLModel) {
        self.cellData = cellData
        self.model = model
    }
    
}
