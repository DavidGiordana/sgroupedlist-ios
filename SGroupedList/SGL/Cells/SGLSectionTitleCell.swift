import UIKit

public class SGLSectionTitleCell: SGLCell {
    
    // MARK: - Static
    
    override class var cellID: String {
        return "SGLSectionTitleCell"
    }
    
    // MARK: - UI Components
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.defaultSecondaryLabel
        return label
    }()
    
    // MARK: - Init
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Settings
    
    override public var theme: SGLTheme! {
        didSet {
            self.backgroundColor = .clear
            self.contentView.backgroundColor = .clear
        }
    }
        
    override public func setData(cellData: SGLCellData, model: SGLModel) {
        super.setData(cellData: cellData, model: model)
        
        guard let cellData = cellData as? SGLSectionTitleCellData else {
            return
        }
        
        self.titleLabel.text = cellData.title
    }
    
    // MARK: - Internal
    
    private func setupUI() {
        self.contentView.addSubview(titleLabel)
        titleLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -4).isActive = true
    }
    
}
