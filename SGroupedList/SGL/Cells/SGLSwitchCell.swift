import UIKit

public class SGLSwitchCell: SGLCell {
    
    // MARK: - Static
    
    override class var cellID: String {
        return "SGLSwitchCell"
    }
    
    // MARK: - UI Components
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.defaultPrimaryLabel
        return label
    }()
    
    private let switchView: UISwitch = {
        let switchControl = UISwitch()
        switchControl.translatesAutoresizingMaskIntoConstraints = false
        return switchControl
    }()
    
    // MARK: - Init
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    // MARK: - Settings
    
    override public var theme: SGLTheme! {
        didSet {
            self.backgroundColor = theme.cellBackgroundColor
            self.contentView.backgroundColor = theme.cellBackgroundColor
            if let tintColor = theme?.tintColor {
                self.switchView.onTintColor = tintColor
            }
        }
    }
    
    override public func setData(cellData: SGLCellData, model: SGLModel) {
        super.setData(cellData: cellData, model: model)
        
        self.selectionStyle = .none
        
        guard let cellData = cellData as? SGLSwitchCellData else {
            return
        }
        
        self.titleLabel.text = cellData.title
        self.switchView.isOn = cellData.value
        
        if let tintColor = theme?.tintColor {
            self.switchView.onTintColor = tintColor
        }
    }
    
    // MARK: - Action
    
    @objc private func onCheckedChange(_ sender: UISwitch) {
        guard
            let cellData = self.cellData as? SGLSwitchCellData,
            let model = super.model as? SGLGroupedModel
        else { return }
        
        cellData.value = sender.isOn
        model.onAction(cell: cellData)
    }
    
    // MARK: - Internal
    
    private func setupUI() {
        self.contentView.addSubview(titleLabel)
        self.titleLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        self.titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0).isActive = true
        self.titleLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
        
        self.contentView.addSubview(switchView)
        self.switchView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        self.switchView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: 0).isActive = true
        
        self.switchView.leftAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: 8).isActive = true
        self.titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 43).isActive = true
        
        self.switchView.addTarget(self, action: #selector(onCheckedChange(_:)), for: .valueChanged)
    }
    
}
