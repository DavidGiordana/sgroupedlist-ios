import UIKit

public class SGLTitleDetailCell: SGLCell {
    
    // MARK: - Static
    
    override class var cellID: String {
         return "SGLTitleDetailCell"
    }
    
    // MARK: - UI Components
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.defaultPrimaryLabel
        return label
    }()
    
    private let detailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.defaultSecondaryLabel
        return label
    }()
        
    // MARK: - Init
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Settings
    
    override public var theme: SGLTheme! {
        didSet {
            guard let theme = self.theme else {
                return
            }
            
            self.backgroundColor = theme.cellBackgroundColor
            self.contentView.backgroundColor = theme.cellBackgroundColor
        }
    }
        
    override public func setData(cellData: SGLCellData, model: SGLModel) {
        super.setData(cellData: cellData, model: model)
        
        guard let cellData = cellData as? SGLTitleDetailCellData else {
            return
        }
        
        self.titleLabel.text = cellData.title
        self.detailLabel.text = cellData.detail
        self.accessoryType = cellData.canGo ? .disclosureIndicator : .none
        self.selectionStyle = cellData.canGo ? .default : .none
    }
    
    // MARK: - Internal
    
    private func setupUI() {
        self.contentView.addSubview(titleLabel)
        self.titleLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        self.titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0).isActive = true
        self.titleLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
        
        self.contentView.addSubview(detailLabel)
        self.detailLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        self.detailLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0).isActive = true
        self.detailLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
        
        self.detailLabel.leftAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: 8).isActive = true
        self.titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 43).isActive = true
    }
    
}
