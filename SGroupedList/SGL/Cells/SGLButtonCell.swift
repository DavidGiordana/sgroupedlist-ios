import UIKit

public class SGLButtonCell: SGLCell {
    
    // MARK: - Static
    
    override class var cellID: String {
         return "SGLButtonCell"
    }
    
    // MARK: - UI Components
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
            
    // MARK: - Init
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Settings
    
    override public var theme: SGLTheme! {
        didSet {
            guard let theme = self.theme else {
                return
            }
            
            self.backgroundColor = theme.cellBackgroundColor
            self.contentView.backgroundColor = theme.cellBackgroundColor
            if let tintColor = theme.tintColor {
                self.titleLabel.textColor = tintColor
            }
        }
    }
        
    override public func setData(cellData: SGLCellData, model: SGLModel) {
        super.setData(cellData: cellData, model: model)
        
        guard let cellData = cellData as? SGLButtonCellData else {
            return
        }
        
        self.titleLabel.text = cellData.title
    }
    
    // MARK: - Internal
    
    private func setupUI() {
        self.contentView.addSubview(titleLabel)
        self.titleLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        self.titleLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        self.titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0).isActive = true
        self.titleLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
        self.titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 43).isActive = true
        self.titleLabel.textColor = theme?.tintColor ?? UIColor.defaultPrimaryLabel
    }
    
}
