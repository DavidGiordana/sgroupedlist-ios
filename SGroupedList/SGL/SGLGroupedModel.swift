import Foundation

public typealias SGLCellAction = (_ model: SGLModel, _ cell: SGLCellData) -> ()

infix operator +++: AdditionPrecedence

infix operator ---: AdditionPrecedence

open class SGLGroupedModel: SGLModel {
    
    // MARK: - Data
    
    // MARK: Private Data
    
    private var items = [SGLCellData]()
    
    // MARK: Private Data - Actions
    
    private var actionsByID = [String: SGLCellAction]()
    
    private var actionsByCellID = [String: SGLCellAction]()
    
    private var actionsByType = [String: SGLCellAction]()
    
    public var deletionConfirmators = [String: SGLCellDeletionConfirmator]()
    
    // MARK: Public Data
    
    public var delegate: SGLModelDelegate?
    
    public var updatesEnabled = true {
        didSet {
            if updatesEnabled {
                self.delegate?.sglUpdateAll()
            }
        }
    }
    
    // MARK: - Init
    
    public init() {}
    
    // MARK: - Interface
    
    public var count: Int {
        return items.count
    }
    
    public subscript(index: Int) -> SGLCellData {
        return items[index]
    }
    
    // MARK: Set
    
    public func set(cells: [SGLCellData]) {
        self.items = cells
        
        delegate?.sglUpdateAll()
    }
    
    // MARK: Add
    
    public func addSection(id: String, title: String) {
        let section = SGLSectionTitleCellData(id: id, title: title)
        
        self.items.append(section)
        
        if updatesEnabled {
            let insertion = SGLModelInsertion([items.count - 1])
            self.delegate?.sglmodelDidInsert(insertions: insertion)
        }
    }
    
    public func addCell(cell: SGLCellData) {
        self.items.append(cell)

        if updatesEnabled {
            let insertion = SGLModelInsertion([items.count - 1])
            self.delegate?.sglmodelDidInsert(insertions: insertion)
        }
    }
    
    public func addCell(atBeginingOfSection sectionID: String, cell: SGLCellData) {
        guard let sectionIdx = getCellIndex(cellID: sectionID) else {
            return
        }
        
        let itemIndex = sectionIdx + 1
        self.items.insert(cell, at: itemIndex)
        
        if updatesEnabled {
            let insertion = SGLModelInsertion([itemIndex])
            self.delegate?.sglmodelDidInsert(insertions: insertion)
        }
    }

    public func addCell(atEndOfSection sectionID: String, cell: SGLCellData) {
        guard let sectionIndex = getCellIndex(cellID: sectionID) else {
            return
        }

        // Search for end of section index
        var indexToInsert = sectionIndex + 1

        for index in indexToInsert..<items.count {
            if (items[index] is SGLSectionTitleCellData) {
                break
            }
            indexToInsert += 1
        }

        self.items.insert(cell, at: indexToInsert)

        if updatesEnabled {
            let insertions = SGLModelInsertion([indexToInsert])
            self.delegate?.sglmodelDidInsert(insertions: insertions)
        }
    }
    
    // MARK:  Remove
    
    public func removeCell(byIndex cellIndex: Int) {
        guard !items.isEmpty else {
            return
        }
        
        self.items.remove(at: cellIndex)
        
        if updatesEnabled {
            let deletions = SGLModelDeletion([cellIndex])
            self.delegate?.sglmodelDidDelete(deletions: deletions)
        }
    }
    
    public func removeCell(byId cellID: String) {
        guard let indexToDelete = getCellIndex(cellID: cellID) else {
           return
        }
              
        self.removeCell(byIndex: indexToDelete)
    }
        
    // MARK: - Update
    
    public func updateCell(withID cellID: String, updapter: @escaping (SGLCellData) -> Bool) {
        guard let cellIndex = self.getCellIndex(cellID: cellID) else {
            return
        }
        
        let cellData = self[cellIndex]
        if updapter(cellData) && updatesEnabled {
            let updates = SGLModelUpdate([cellIndex])
            self.delegate?.sglmodelDidUpdate(updates: updates)
        }
    }
    
    public func updateCell(at cellIndex: Int) {
        if updatesEnabled {
            let updates = SGLModelUpdate([cellIndex])
            self.delegate?.sglmodelDidUpdate(updates: updates)
        }
    }
    
    public func updateCell(withID cellID: String) {
        guard let index = items.firstIndex(where: { $0.id == cellID }) else {
            return
        }
        
        self.updateCell(at: index)
    }
    
    // MARK:  Action
    
    public func addActionByID(actionID: String, action: @escaping SGLCellAction) {
        self.actionsByID[actionID] = action
    }
    
    public func addActionByCellID(cellID: String, action: @escaping SGLCellAction) {
        self.actionsByCellID[cellID] = action
    }
    
    public func addActionByType <T: SGLCellData> (clazz: T.Type, action: @escaping SGLCellAction) {
        let type = String(describing: T.self)
        self.actionsByType[type] = action
    }
    
    public func onAction(cell: SGLCellData) {
        if let actionID = cell.actionID, let action = actionsByID[actionID] {
            action(self, cell)
            return
        }
        
        if let cellID = cell.id, let action = actionsByCellID[cellID] {
            action(self, cell)
            return
        }
        
        let cellType = String(describing: type(of: cell))
        self.actionsByType[cellType]?(self, cell)
    }
    
    // MARK: - Operators
    
    // MARK: Add
    
    public static func +++ (model: SGLGroupedModel, sectionData: (id: String, title: String)) {
        model.addSection(id: sectionData.id, title: sectionData.title)
    }
    
    public static func +++ (model: SGLGroupedModel, cell: SGLCellData) {
        model.addCell(cell: cell)
    }
    
    // MARK:  Remove
    
    public static func --- (model: SGLGroupedModel, cellIndex: Int) {
        model.removeCell(byIndex: cellIndex)
    }
    
    public static func --- (model: SGLGroupedModel, cellID: String) {
        model.removeCell(byId: cellID)
    }
    
    // MARK: - Internal
    
    private func getCellIndex(cellID: String) -> Int? {
        for (index, item) in items.enumerated() {
            if item.id == cellID {
                return index
            }
        }
        
        return nil
    }
    
}
