import Foundation

// MARK: - Delegate

public protocol SGLModelDelegate {
    
    func sglmodelDidInsert(insertions: SGLModelInsertion)
    
    func sglmodelDidDelete(deletions: SGLModelDeletion)
    
    func sglmodelDidUpdate(updates: SGLModelUpdate)
    
    func sglUpdateAll()
        
}

public struct SGLModelInsertion {
    
    // MARK: - Data
    
    let indexes: [Int]
    
    // MARK: - Init
    
    init(_ indexes: [Int]) {
        self.indexes = indexes
    }
    
}

public struct SGLModelDeletion {
    
    // MARK: - Data
    
    let indexes: [Int]
    
    // MARK: - Init
    
    init(_ indexes: [Int]) {
        self.indexes = indexes
    }
    
}

public struct SGLModelUpdate {
    
    // MARK: - Data
    
    let indexes: [Int]
    
    // MARK: - Init
    
    init(_ indexes: [Int]) {
        self.indexes = indexes
    }
    
}
