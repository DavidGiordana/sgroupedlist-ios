import UIKit

import UIKit

public typealias CellRegistrator = (UITableView) -> ()

open class SGLDataSource: NSObject, SGLThemeSettable {
    
    // MARK: - UI
    
    public var tableView: UITableView! {
        didSet {
            self.setupIfPossible()
        }
    }
    
    private weak var emptyTableLabel: UILabel!
    
    // MARK: - Data
    
    public var model: SGLModel! {
        didSet {
            oldValue?.delegate = nil
            self.setupIfPossible()
        }
    }
    
    public var theme: SGLTheme! = SGLTheme() {
        didSet {
            guard let tableView = self.tableView else {
                return
            }
            
            for cell in tableView.visibleCells {
                if let cell = cell as? SGLThemeSettable {
                    cell.theme = self.theme
                }
            }
            
            self.tableView.backgroundColor = theme.tableViewColor
        }
    }
    
    public var emptyTableMessage: String? {
        didSet {
            self.emptyTableLabel?.text = self.emptyTableMessage
        }
    }
    
    var registrators = [CellRegistrator]()
    
    // MARK: - Init
    
    override public init() {
        super.init()
        
        self.registerCell { (tableView: UITableView) in
            tableView.register(SGLSectionTitleCell.self, forCellReuseIdentifier: SGLSectionTitleCell.cellID)
        }
        self.registerCell { (tableView: UITableView) in
            tableView.register(SGLTitleDetailCell.self, forCellReuseIdentifier: SGLTitleDetailCell.cellID)
        }
        self.registerCell { (tableView: UITableView) in
            tableView.register(SGLSwitchCell.self, forCellReuseIdentifier: SGLSwitchCell.cellID)
        }
        self.registerCell { (tableView: UITableView) in
            tableView.register(SGLButtonCell.self, forCellReuseIdentifier: SGLButtonCell.cellID)
        }
    }
    
    // MARK: - Interface
    
    // MARK: Register Cells
    
    public func registerCell(registrator: @escaping CellRegistrator) {
        if let tableView = self.tableView {
            registrator(tableView)
        }
        
        self.registrators.append(registrator)
    }
    
    // MARK: - Internal
    
    private func setupIfPossible() {
        guard let tableView = self.tableView, self.model != nil else { return }
        
        self.addEmptyTableLabel()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = theme.tableViewColor
        
        for registrator in self.registrators {
            registrator(tableView)
        }
        
        model.delegate = self
    }
    
    private func addEmptyTableLabel() {
        guard let tableView = self.tableView else {
            return
        }
        
        emptyTableLabel?.removeFromSuperview()
        
        guard let view = tableView.superview else {
            return
        }
        
        let emptyLabel = UILabel()
        emptyLabel.textColor = .defaultSecondaryLabel
        emptyLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        emptyLabel.textAlignment = .center
        emptyLabel.numberOfLines = 2
        emptyLabel.text = self.emptyTableMessage
        emptyLabel.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(emptyLabel)
        
        emptyLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        emptyLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        emptyLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        emptyTableLabel = emptyLabel
    }
    
}

// MARK: - UITableViewDataSource

extension SGLDataSource: UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.emptyTableLabel?.isHidden = model.count != 0
        
        return model.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.emptyTableLabel?.isHidden = true
        
        let cellData = model[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellData.cellID, for: indexPath)
        
        if let cell = cell as? SGLCell {
            cell.setData(cellData: cellData, model: model)
            cell.theme = theme
        }
        
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension SGLDataSource: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let cellData = model[indexPath.row]
        return cellData.deletable != .nonDeletable
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.onDelete(cellRow: indexPath.row)
        }
    }
    
    // MARK: Internal
    
    private func onDelete(cellRow: Int) {
        let cellData = self.model[cellRow]
        
        switch cellData.deletable {
        case .nonDeletable: break
        case .deletable:
            model.removeCell(byIndex: cellRow)
        case .deletableWithConfirmation(let confirmationID):
            onDeleteWithConfirmation(cellRow: cellRow, confirmationID: confirmationID)
        }
    }
    
    private func onDeleteWithConfirmation(cellRow: Int, confirmationID: String) {
        guard let confirmator = model.deletionConfirmators[confirmationID] else { return }
        
        let cellData = self.model[cellRow]
        
        confirmator(cellData, cellRow) {
            self.model.removeCell(byIndex: cellRow)
        }
    }
    
}


// MARK: - SGLModelDelegate

extension SGLDataSource: SGLModelDelegate {
    
    public func sglmodelDidInsert(insertions: SGLModelInsertion) {
        let sortedItems = insertions.indexes.sorted { $0 > $1 }
        let indexes = sortedItems.map { IndexPath(row: $0, section: 0) }
        self.tableView?.insertRows(at: indexes, with: .automatic)
    }
    
    public func sglmodelDidDelete(deletions: SGLModelDeletion) {
        let sortedItems = deletions.indexes.sorted { $0 > $1 }
        let indexes = sortedItems.map { IndexPath(row: $0, section: 0) }
        self.tableView?.deleteRows(at: indexes, with: .automatic)
    }
    
    public func sglmodelDidUpdate(updates: SGLModelUpdate) {
        let sortedItems = updates.indexes.sorted { $0 > $1 }
        let indexes = sortedItems.map { IndexPath(row: $0, section: 0) }
        self.tableView?.reloadRows(at: indexes, with: .automatic)
    }
    
    public func sglUpdateAll() {
        self.tableView?.reloadData()
    }
    
}
