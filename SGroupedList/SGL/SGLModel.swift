import Foundation

public typealias SGLCallback = () -> Void

public typealias SGLCellDeletionConfirmator = (SGLCellData, Int, @escaping SGLCallback) -> Void

public protocol SGLModel: class {
    
    var delegate: SGLModelDelegate? { get set }
    
    var count: Int { get }
    
    subscript(index: Int) -> SGLCellData { get }
    
    var deletionConfirmators: [String: SGLCellDeletionConfirmator] { get set }
    
    func updateCell(at cellIndex: Int)
    
    func updateCell(withID cellID: String)
    
    func removeCell(byIndex cellIndex: Int)
    
}

extension SGLModel {
    
    public func addRemoveConfirmator(cellID: String, confirmator: @escaping SGLCellDeletionConfirmator) {
        self.deletionConfirmators[cellID] = confirmator
    }
    
}
