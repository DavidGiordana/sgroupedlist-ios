import UIKit

open class SGLSimpleDataSource: SGLDataSource {
    
    public weak var delegate: SGLSimpleDataSourceDelegate?
    
}

// MARK: - UITableViewDelegate

extension SGLSimpleDataSource {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        delegate?.sglSimpleDataSource(onSelectOfElementAt: indexPath.row)
    }
    
}

public protocol SGLSimpleDataSourceDelegate: class {
    
    func sglSimpleDataSource(onSelectOfElementAt elementIndex: Int)
    
}
