import Foundation

infix operator <=>: AdditionPrecedence

public protocol Matchable {
    
    static func <=> (lhs: Self, rhs: Self) -> Bool
    
}

open class SGLSimpleModel<Element: Matchable>: SGLModel {
    
    public typealias SGLSimpleModelTransformer = (Element) -> SGLCellData
    
    // MARK: - Data
    
    // MARK: Private Data
    
    private var items = [Element]()
    
    private var transformedItems = [SGLCellData]()
    
    // MARK: Public Data
    
    public var deletionConfirmators = [String: SGLCellDeletionConfirmator]()
    
    public var transformer: SGLSimpleModelTransformer!
    
    public var delegate: SGLModelDelegate?
    
    public var updatesEnabled = true {
        didSet {
            if updatesEnabled {
                self.delegate?.sglUpdateAll()
            }
        }
    }
    
    // MARK: - Init
    
    public init() {}
    
    // MARK: - Interface
    
    public var count: Int {
        return items.count
    }
    
    public subscript(index: Int) -> SGLCellData {
        return transformedItems[index]
    }
    
    // MARK: Set
    
    public func set(elements: [Element]) {
        self.items = elements
        self.transformedItems = elements.map(transformer)
        
        if updatesEnabled {
            delegate?.sglUpdateAll()
        }
    }
    
    // MARK: - Get
    
    public func get(elementAt index: Int) -> Element {
        return items[index]
    }
    
    // MARK: Add
    
    public func add(element: Element) {
        guard let transformer = self.transformer else {
            return
        }
        
        let transformedElement = transformer(element)
        
        self.items.append(element)
        self.transformedItems.append(transformedElement)
        
        if updatesEnabled {
            let insertion = SGLModelInsertion([items.count - 1])
            self.delegate?.sglmodelDidInsert(insertions: insertion)
        }
    }
    
    public func insert(element: Element, at index: Int) {
        guard
            let transformer = self.transformer,
            index > 0,
            index <= items.count
        else { return }
        
        let transformedElement = transformer(element)
        
        self.items.insert(element, at: index)
        self.transformedItems.insert(transformedElement, at: index)
        
        if updatesEnabled {
            let insertion = SGLModelInsertion([index])
            self.delegate?.sglmodelDidInsert(insertions: insertion)
        }
    }
    
    // MARK:  Remove
    
    public func removeCell(byIndex cellIndex: Int) {
        self.removeElement(byIndex: cellIndex)
    }
    
    public func removeElement(byIndex elementIndex: Int) {
        guard elementIndex >= 0, elementIndex < items.count else {
            return
        }
        
        self.items.remove(at: elementIndex)
        self.transformedItems.remove(at: elementIndex)
        
        if updatesEnabled {
            let deletions = SGLModelDeletion([elementIndex])
            self.delegate?.sglmodelDidDelete(deletions: deletions)
        }
    }
    
    public func remove(element: Element) {
        guard let elementIndex = items.firstIndex(where: { $0 <=> element }) else {
            return
        }
        
        self.removeElement(byIndex: elementIndex)
    }
    
    // MARK: - Update
    
    public func update(element: Element, atIndex elementIndex: Int) {
        self.items[elementIndex] = element
        self.transformedItems[elementIndex] = transformer(element)
        
        self.updateCell(at: elementIndex)
    }
    
    public func update(element: Element, onFirstMatch matcher: (Element) -> Bool) {
        guard let index = self.items.firstIndex(where: matcher) else {
            return
        }
        
        self.update(element: element, atIndex: index)
    }
    
    public func updateCell(at cellIndex: Int) {
        if updatesEnabled {
            let updates = SGLModelUpdate([cellIndex])
            self.delegate?.sglmodelDidUpdate(updates: updates)
        }
    }
    
    public func updateCell(withID cellID: String) {
        guard let index = transformedItems.firstIndex(where: { $0.id == cellID }) else {
            return
        }
        
        self.updateCell(at: index)
    }
    
    // MARK: - Operators
    
    // MARK: Add
    
    public static func +++ (model: SGLSimpleModel, element: Element) {
        model.add(element: element)
    }
    
    // MARK:  Remove
    
    public static func --- (model: SGLSimpleModel, elementIndex: Int) {
        model.removeElement(byIndex: elementIndex)
    }
    
    public static func --- (model: SGLSimpleModel, element: Element) {
        model.remove(element: element)
    }
    
}
