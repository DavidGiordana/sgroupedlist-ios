import UIKit

open class SGLGroupedDataSource: SGLDataSource { }

// MARK: - UITableViewDelegate

extension SGLGroupedDataSource {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let model = self.model as? SGLGroupedModel else {
            return
        }
        
        let cellData = self.model[indexPath.row]
        model.onAction(cell: cellData)
    }
    
}
