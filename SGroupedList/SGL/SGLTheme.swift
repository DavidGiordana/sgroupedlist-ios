import UIKit

public struct SGLTheme {
 
    // MARK: - Properties
    
    public var tableViewColor = UIColor.defaultTableViewColor
    
    public var cellBackgroundColor = UIColor.defaultCellBackgroundColor
    
    public var tintColor: UIColor?

}

public protocol SGLThemeSettable: class {
    
    var theme: SGLTheme! { get set }
    
}

public extension UIColor {
    
    static var defaultPrimaryLabel: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.label
        } else {
            return .darkText
        }
    }
    
    static var defaultSecondaryLabel: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.secondaryLabel
        } else {
            return .lightText
        }
    }
    
    static var defaultTableViewColor: UIColor {
       if #available(iOS 13.0, *) {
           return .systemGroupedBackground
       } else {
           return .groupTableViewBackground
       }
   }
       
   static var defaultCellBackgroundColor: UIColor {
       if #available(iOS 13.0, *) {
           return .secondarySystemGroupedBackground
       } else {
           return .white
       }
   }
    
}


