import Foundation

public class SGLButtonCellData: SGLCellData {
    
    // MARK: - Data
    
    public var cellID: String {
       return SGLButtonCell.cellID
   }
    
    public let id: String?
    
    public let title: String
        
    public let actionID: String?
    
    public let deletable: Deletable
    
    public let canGo: Bool = false
        
    // MARK: - Init
    
    public init(id: String? = nil, title: String, detail: String? = nil, actionID: String? = nil, deletable: Deletable = .nonDeletable) {
        self.id = id
        self.title = title
        self.actionID = actionID
        self.deletable = deletable
    }
    
}
