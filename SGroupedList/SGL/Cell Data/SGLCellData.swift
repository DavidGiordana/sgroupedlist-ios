import Foundation

public protocol SGLCellData {
    
    // MARK: - Properties
    
    var id: String? { get }
        
    var actionID: String? { get }
    
    var cellID: String { get }
    
    var deletable: Deletable { get }
    
    var canGo: Bool { get }
    
}

// MARK: - Deletable

public enum Deletable: Equatable {
    case nonDeletable
    case deletable
    case deletableWithConfirmation(confirmationID: String)
}
