import Foundation

public class SGLTitleDetailCellData: SGLCellData {
    
    // MARK: - Data
    
    public var cellID: String {
       return SGLTitleDetailCell.cellID
   }
    
    public let id: String?
    
    public let title: String
    
    public var detail: String?
    
    public let actionID: String?
    
    public let deletable: Deletable
    
    public let canGo: Bool
        
    // MARK: - Init
    
    public init(id: String? = nil, title: String, detail: String? = nil, actionID: String? = nil, deletable: Deletable = .nonDeletable, canGo: Bool = false) {
        self.id = id
        self.title = title
        self.actionID = actionID
        self.detail = detail
        self.deletable = deletable
        self.canGo = canGo
    }
    
}
