import Foundation

public class SGLSectionTitleCellData: SGLCellData {
    
    // MARK: - Data
    
    public var cellID: String {
        return SGLSectionTitleCell.cellID
    }
    
    public let id: String?
    
    public let title: String
    
    public let actionID: String?
    
    public let deletable = Deletable.nonDeletable
    
    public let canGo = false
    
    // MARK: - Init
    
    public init(id: String, title: String, actionID: String? = nil) {
        self.id = id
        self.title = title
        self.actionID = actionID
    }
    
}
