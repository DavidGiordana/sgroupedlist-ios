import Foundation

public class SGLSwitchCellData: SGLCellData {
    
    // MARK: - Data
    
    public var cellID: String {
        return SGLSwitchCell.cellID
    }
    
    public let id: String?
    
    public let title: String
    
    public let actionID: String?
    
    public var value: Bool
    
    public let deletable = Deletable.nonDeletable
    
    public let canGo: Bool = false
    
    // MARK: - Init
    
    public init(id: String?, title: String, actionID: String? = nil, value: Bool) {
        self.id = id
        self.title = title
        self.actionID = actionID
        self.value = value
    }
    
}
